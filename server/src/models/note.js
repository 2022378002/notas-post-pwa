const mongoose = require('mongoose')

const noteSchema = mongoose.Schema({
    id: {
        type: String,
        required: true
    },
    note: {
        type: String,
        required: true
    },
    noteDone: {
        type: Boolean,
        required: true
    }

})
module.exports = mongoose.model('Note', noteSchema)